<?php
/**
 * Docker multiple containers demo.
 *
 * Demonstrates connection to a mysql server running on other container.
 */
ini_set('display_errors', 1);
error_reporting(-1);

// Connect to database.
$conn = new mysqli('db', 'new_user', 'new_pass', 'new_db');
if ($conn->connect_error) {
    die('Connection failed: ' . $conn->connect_error);
}

// Create table and populate.
$conn->query('DROP TABLE IF EXISTS `users`');
$conn->query(
  'CREATE TABLE `users` (
     `id` int(11) NOT NULL AUTO_INCREMENT,
     `name` varchar(20) NOT NULL,
     PRIMARY KEY (`id`)
  )'
);
$conn->query("INSERT INTO `users` VALUES (1, 'user1'),(2, 'user2')");

// List table content.
echo "<h3>Users</h3>";

if ($result = $conn->query('SELECT * FROM `users`')) {
    while ($row = $result->fetch_object()) {
        echo $row->id . ' - ' . $row->name . '<br />';
    }
} else {
    echo 'No entries found...';
}

$conn->close();
