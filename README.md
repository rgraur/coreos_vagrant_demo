# CoreOS & Vagrant demo #

### Requirements ###
* see [CoreOS & Vagrant web environment](https://bitbucket.org/rgraur/coreos_vagrant_env) requirements section

### Install ###
* clone repo `git clone git@bitbucket.org:rgraur/coreos_vagrant_demo.git && cd coreos_vagrant_demo`
* see [CoreOS & Vagrant web environment](https://bitbucket.org/rgraur/coreos_vagrant_env) install section

### Run ###
* see [CoreOS & Vagrant web environment](https://bitbucket.org/rgraur/coreos_vagrant_env) run section
